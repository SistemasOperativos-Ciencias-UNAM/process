/* http://www.yolinux.com/TUTORIALS/ForkExecProcesses.html */

#include <stdio.h>
#include <stdlib.h>

int main(void)
{
  int salida;

  /* El argumento de system(3) se pasa a /bin/sh -c COMANDO */
  salida = system("uname -a ; date ; sleep 60 ;");

  printf("Codigo de salida de system(3): %d\n", salida);
  return 0;
}
