#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>

/* Define constantes globales */
#define NON_BLOCK
#define SLEEP
#define ARCHIVO "/tmp/archivo.txt"
#define BUF_LEN 255

/* Mensaje a imprimir */
char *MENSAJE = "aeiou";
// char *MENSAJE = "abcdefghijklmnopqrstuvwxyz";
// char *MENSAJE = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
// char *MENSAJE = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

/* Imprime un mensaje de error y sale el programa */
void error(char * msg)
{
  perror(msg);
  exit(EXIT_FAILURE);
}

/* Función principal */
int main(void)
{
  /* Borra el archivo si existe */
  struct stat sb;
  if(stat(ARCHIVO, &sb) == 0)
    truncate(ARCHIVO, 0);

  /* Abre ARCHIVO y regresa un descriptor de archivo */
  int arch_padre_fd = open(ARCHIVO, O_RDWR|O_TRUNC|O_CREAT, S_IRWXU|S_IRGRP|S_IROTH);

  /* Utiliza el descriptor de archivo para obtener un apuntador a archivo */
  FILE * arch_padre_fp = fdopen(arch_padre_fd, "w+");
  /* Regresa el apuntador de posición al inicio del archivo */
  rewind(arch_padre_fp);

  /* Crea un proceso hijo */
  pid_t pid = fork();

  /* Error en la creación del proceso hijo */
  if (pid < 0)
    error("Error en fork(2)");

  /* Código que ejecuta el proceso padre */
  if (pid > 0)
  {
    /* Obtiene los identificadores de proceso */
    pid_t pid_padre = getpid();
    pid_t pid_hijo = pid;

    /* Inicializa el búffer con '\0' */
    char buffer[BUF_LEN] = "";
    bzero(buffer, BUF_LEN);

    /* Imprime una línea de identificación */
    printf("[padre]\t(pid_padre: %d) (pid_hijo: %d)\n", pid_padre, pid_hijo);
    fprintf(stderr, "[padre]\t\tImprimiendo líneas en: %s\n", ARCHIVO);

    /* Escribe un mensaje de manera incremental en el archivo abierto */
    for(int offset=1 ; offset<=strlen(MENSAJE) ; offset++)
    {
      /* Inicializa el buffer con '\0' */
      bzero(buffer, BUF_LEN);
      /* Copia una parte de MENSAJE hasta offset */
      strncpy(buffer, MENSAJE, offset);
      /* Agrega un retorno de línea al final de la cadena */
      buffer[offset] = '\n';
      /* Escribe en el pipe el bloque de caracteres buffer */
      write(arch_padre_fd, buffer, BUF_LEN);
    }
    /* Forza la escritura en todos los archivos abiertos */
    fflush(NULL);
    /* Cierra el descriptor de archivo */
    close(arch_padre_fd);
    /* Espera a que el proceso hijo termine */
    wait(NULL);
    /* Borra el archivo */
    unlink(ARCHIVO);
  }

  /* Código que ejecuta el proceso hijo */
  else /* pid == 0 */
  {
    /* Obtiene los identificadores de proceso */
    pid_t pid_padre = getppid();
    pid_t pid_hijo = getpid();

    /* Inicializa el búffer con '\0' */
    char buffer[BUF_LEN] = "";
    bzero(buffer, BUF_LEN);

    /* Duplica el descriptor de archivo */
    int arch_hijo_fd = dup(arch_padre_fd);
    /* Cierra el descriptor de archivo original */
    close(arch_padre_fd);

    /* Utiliza el descriptor de archivo para obtener un apuntador a archivo */
    FILE * arch_hijo_fp = fdopen(arch_hijo_fd, "r+");
    /* Regresa el apuntador de posición al inicio del archivo */
    rewind(arch_hijo_fp);

    /* Imprime una línea de identificación */
    printf("[hijo]\t(pid_padre: %d) (pid_hijo: %d)\n", pid_padre, pid_hijo);
    fprintf(stderr, "[hijo]\t\tLeyendo líneas desde: %s\n", ARCHIVO);

    /* Espera a que el proceso padre termine de escribir en el archivo */
    sleep(1);

    /* Lee línea a línea el contenido del archivo abierto */
    int i = 1;
    ssize_t n = read(arch_hijo_fd, buffer, BUF_LEN);
    while (n > 1)
    {
#ifdef SLEEP
      /* Inserta una pausa */
      sleep(1);
#endif

      /* Imprime el contenido de la línea que se guardó en el búffer */
      printf("[hijo]\tbuffer: (%d)\t%s", i, buffer);
      /* Limpia el buffer despues de usarlo */
      bzero(buffer, BUF_LEN);
      i++;
      /* Lee del pipe el siguiente bloque de caracteres */
      n = read(arch_hijo_fd, buffer, BUF_LEN);
    }

    /* Cierra el archivo */
    close(arch_hijo_fd);
  }

  return EXIT_SUCCESS;
}
